#
# Copyright (c) 2016-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree. An additional grant
# of patent rights can be found in the PATENTS file in the same directory.
#

OS := $(shell uname)
ifeq ($(OS),Darwin)
  	# Run MacOS commands
  	CXX = icpc
	CXXFLAGS = -g3 -O3 -pthread -std=c++11 -Wall -openmp
	LINKERFLAGS = -lgfortran -mkl
else
	# check for Linux and run other commands
	CXX = g++
	CXXFLAGS = -g3 -O3 -pthread -std=c++11 -Wall -fopenmp
	LINKERFLAGS = -lgfortran -lblas -fopenmp
endif

# -Wc++11-extensions
OBJS = args.o dictionary.o matrix.o vector.o utils.o 
INCLUDES = -I. -Ifasttext/

FASTTEXT = fasttext/args.o fasttext/dictionary.o

all: CXXFLAGS += -mtune=native -march=native
all: propack fasttext svd test
	${CXX} ${CXXFLAGS} ${INCLUDES} test.o matrix.o svd.o propack/*.o propack/Lapack_Util/*.o ${FASTTEXT} ${LINKERFLAGS} -o doc2vex

debug: CXXFLAGS += -g -O0 -fno-inline
debug: all

.PHONY: fasttext propack test

test: 
	echo ${INCLUDES}
	${CXX} ${CXXFLAGS} ${INCLUDES} -c test.cpp matrix.cpp

svd: 
	${CXX} ${CXXFLAGS} ${INCLUDES}  -c svd.cpp

fasttext: 
	cd fasttext && $(MAKE)
propack: 
	cd propack && $(MAKE) lib

clean:
	rm -rf *.o

clean-all: clean
	cd propack && $(MAKE) clean
	cd fasttext && $(MAKE) clean

valgrind: debug
	valgrind --leak-check=full ./doc2vex supervised -input supersimple.txt -lambda 0 -epoch 1000  -minCount 0 -softmaxSample 0 -initialRank 11  
	#valgrind --leak-check=full ./doc2vex supervised -input amazonReviews.txt -minCount 5 
