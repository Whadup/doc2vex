#include "matrix.h"
#include <cmath>
#include <cassert>
#include <iostream>
void duplicateSparseMatrix(SparseMatrix& orig, SparseMatrix& copy)
{
	copy.zero = orig.zero;
	copy.documentSizes.insert(copy.documentSizes.begin(),orig.documentSizes.begin(),orig.documentSizes.end());
	copy.tokenIds.insert(copy.tokenIds.begin(),orig.tokenIds.begin(),orig.tokenIds.end());
	copy.tokenFrequencies.insert(copy.tokenFrequencies.begin(),orig.tokenFrequencies.begin(),orig.tokenFrequencies.end());
	copy.tokenSizes.insert(copy.tokenSizes.begin(),orig.tokenSizes.begin(),orig.tokenSizes.end());
	copy.documentIds.insert(copy.documentIds.begin(),orig.documentIds.begin(),orig.documentIds.end());
	copy.documentFrequencies.insert(copy.documentFrequencies.begin(),orig.documentFrequencies.begin(),orig.documentFrequencies.end());
}

void makeLowRankMatrix(LowRankMatrix& l, int m, int n, int k)
{
	l.m=m;
	l.n=n;
	l.k=k;
	l.S = new double[k];
	// std::cout << "kmax+1 "<< (3*k+1) << std::endl;
	l.U = new double[(3*k+1)*m];
	// std::cout << l.U << std::endl;
	l.V = new double[3*k*n];
}

void deleteLowRankMatrix(LowRankMatrix& l)
{
	if(l.S) delete[] l.S;
	if(l.U) delete[] l.U;
	if(l.V) delete[] l.V;
}

void sparseMatrixVector(SparseMatrix& s,double* x, double* y,bool transposed, bool add)
{
	if(!transposed)
	{
		//m rows (tokens)
		//n columns (documents)
		double sum = 0;
		for(int i=0;i<s.documentSizes.size()-1;i++)
			sum+=x[i]*s.zero[i];
		#pragma omp parallel for
		for(int i=0;i<s.tokenSizes.size()-1;i++)
		{
			if(!add)
				y[i] = 0;
			y[i] += sum;
			for(int j=s.tokenSizes[i];j<s.tokenSizes[i+1];j++)
				y[i]+=(s.documentFrequencies[j]-s.zero[s.documentIds[j]])*x[s.documentIds[j]];
		}
	}
	else
	{
		double sum = 0;
		for(int i=0;i<s.tokenSizes.size()-1;i++)
			sum+=x[i];
		#pragma omp parallel for
		for(int i=0;i<s.documentSizes.size()-1;i++)
		{
			if(!add)
				y[i] = 0;
			y[i] += sum*s.zero[i];
			for(int j=s.documentSizes[i];j<s.documentSizes[i+1];j++)
				y[i]+=(s.tokenFrequencies[j]-s.zero[i])*x[s.tokenIds[j]];
		}
	}
}

void lowRankMatrixVector(LowRankMatrix& l,double scale,double* x, double* y,bool transposed, bool add)
{
	double* buffer = new double[l.k];//{0.0};
	// std::cout << d->documentSizes->size() << std::endl;
	if(!transposed)
	{
		//US(V^Tx)
		#pragma omp parallel for
		for(int i=0;i<l.k;i++)
		{				
			buffer[i] = 0;
			for(int j=0;j<l.n;j++)
			{
				buffer[i]+=scale*l.S[i]*l.V[i*l.n+j]*x[j];
			}
		}
		#pragma omp parallel for
		for(int i=0;i<l.m;i++)
		{
			if(!add)
				y[i]=0;
			for(int j=0;j<l.k;j++)
			{
				y[i]+=buffer[j]*l.U[j*l.m+i];
			}
		}
	}
	else
	{
		//this is tricky...
		//(USV^T)^T = VSU^T
		#pragma omp parallel for
		for(int i=0;i<l.k;i++)
		{	
			buffer[i] = 0;
			for(int j=0;j<l.m;j++)
			{
				buffer[i]+=scale*l.S[i]*l.U[i*l.m+j]*x[j];
			}
		}
		//this is sparse
		#pragma omp parallel for
		for(int i=0;i<l.n;i++)
		{
			if(!add)
				y[i]=0;
			for(int j=0;j<l.k;j++)
			{
				y[i]+=buffer[j]*l.V[j*l.n+i];
			}
		}
	}
	delete[] buffer;
}


void reTranspose(int ii,int nwords,SparseMatrix& d)
{
	int* tmp = new int[nwords];
	for(int i=0;i<nwords;i++)
		tmp[i]=0;

	for(int i=0;i<ii;i++)
	{
		for(int j = d.documentSizes[i];j<d.documentSizes[i+1];j++)
		{
			int token = d.tokenIds[j];
			assert(token<nwords);
			double freq = d.tokenFrequencies[j];
			d.documentIds[d.tokenSizes[token]+tmp[token]] = i;
			d.documentFrequencies[d.tokenSizes[token]+tmp[token]] = freq;
			tmp[token]++;
		}
	}
	delete[] tmp;
}

/*
 * Compute sum_i exp p[i]
 */
double logSum(LowRankMatrix& l, int doc,double* buffer, double& minValue)
{
	double maxTerm = 0;

	for(int i=0;i < l.m;i++)
	{
		buffer[i]=0;
		for(int k=0;k<l.k;k++)
			buffer[i]+=l.S[k]*l.U[i+l.m*k] * l.V[doc+l.n*k];
			// l.U[i+k*l.m] * l.S[k] * l.V[k+doc*l.k]; //omg wie ist das ding indexiert?
		if(buffer[i]>maxTerm || i==0)
			maxTerm = buffer[i];
		if(buffer[i] < minValue)
			minValue = buffer[i];
	}
			
	double logSum = 0;
	for(int i=0;i<l.m;i++)
		logSum += exp(buffer[i] - maxTerm);
	return maxTerm + log(logSum);
}

/*
 * Compute sum_i exp p[i]
 */
double sampledLogSum(LowRankMatrix& l, int doc,double* buffer, double& minValue,int sampleSize, int* sample)
{
	double maxTerm = 0;

	#pragma omp parallel for
	for(int i=0;i < sampleSize;i++)
	{
		int j = sample[i];
		buffer[i]=0;
		for(int k=0;k<l.k;k++)
			buffer[i]+=l.S[k]*l.U[j+l.m*k] * l.V[doc+l.n*k];
		// buffer[i]+=l.S[k]*l.U[i*l.k+k] * l.V[doc*l.k+k];
	}
	for(int i=0;i < sampleSize;i++)
	{
		if(buffer[i]>maxTerm || i==0)
			maxTerm = buffer[i];
		if(buffer[i] < minValue)
			minValue = buffer[i];
	}
			
	double logSum = 0;
	for(int i=0;i<sampleSize;i++)
		logSum += exp(buffer[i] - maxTerm);
	return maxTerm + log(logSum);
}

double logSumAccelerated(LowRankMatrix& l, LowRankMatrix& l2, double a,double b,int doc,double* buffer, double& minValue)
{
	double maxTerm = 0;

	for(int i=0;i < l.m;i++)
	{
		buffer[i]=0;
		for(int k=0;k<l.k;k++)
			buffer[i]+=a *  l.S[k] * l.U[i+l.m*k] *  l.V[doc+l.n*k];
		for(int k=0;k<l2.k;k++)
			buffer[i]+=b * l2.S[k] *l2.U[i+l2.m*k] * l2.V[doc+l2.n*k];
		
			// l.U[i+k*l.m] * l.S[k] * l.V[k+doc*l.k]; //omg wie ist das ding indexiert?
		if(buffer[i]>maxTerm || i==0)
			maxTerm = buffer[i];
		if(buffer[i] < minValue)
			minValue = buffer[i];
	}
			
	double logSum = 0;
	for(int i=0;i<l.m;i++)
		logSum += exp(buffer[i] - maxTerm);
	return maxTerm + log(logSum);
}



double sampledLogSumAccelerated(LowRankMatrix& l, LowRankMatrix& l2, double a,double b, int doc,double* buffer, double& minValue,int sampleSize, int* sample)
{
	double maxTerm = 0;

	#pragma omp parallel for
	for(int i=0;i < sampleSize;i++)
	{
		int j = sample[i];
		buffer[i]=0;
		for(int k=0;k<l.k;k++)
			buffer[i]+=a *  l.S[k] *  l.U[j+l.m*k] *  l.V[doc+l.n*k];
		for(int k=0;k<l2.k;k++)
			buffer[i]+=b * l2.S[k] * l2.U[j+l2.m*k] * l2.V[doc+l2.n*k];
		
		// buffer[i]+=l.S[k]*l.U[i*l.k+k] * l.V[doc*l.k+k];
	}
	for(int i=0;i < sampleSize;i++)
	{
		if(buffer[i]>maxTerm || i==0)
			maxTerm = buffer[i];
		if(buffer[i] < minValue)
			minValue = buffer[i];
	}
			
	double logSum = 0;
	for(int i=0;i<sampleSize;i++)
		logSum += exp(buffer[i] - maxTerm);
	return maxTerm + log(logSum);
}