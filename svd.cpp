#include "svd.h"
#include <iostream>
int svd(int m, 
	int n, 
	int k,
	void (*aprod)(	const char *transa,
					int *m,
					int *n,
					double *x,
					double *y,
					double *dparm,
					int *iparm),
	double* U,
	double* V,
	double* pS,
	double anorm,
	void* user
	)
{
	char YES = 'y';
	int N=n;
	int M=m;
	int K=k;
	int KMAX = 3*k - 1;
	// std::cout << "KMAX" <<KMAX << std::endl;
	
	int LDU = M; 
	//#warning "ldu and ldv not set"
	//double* U = new double[LDU*(KMAX+1)];
	for(int i=0;i<LDU*(KMAX+1);i++)
		U[i]=0;

	double* BND = new double[K];

	
	int LDV = N;
	//double* V = new double[LDV*(KMAX)];

	char e = 'e';
	double eps = dlamch_(&e);
	
	double TOLIN = 16*eps;
	// std::cout << eps << " " << TOLIN << std::endl;
	//should be at least M + N + 9*KMAX + 5*KMAX**2 + 4 + MAX(3*KMAX**2+4*KMAX+4, NB*MAX(M,N))
	int LWORK = M + N + 10*KMAX + 6*KMAX*KMAX + 4 + max(3*KMAX*KMAX+4*KMAX+4, 2*max(M,N));
	double* WORK = new double[LWORK];
	int LIWORK = 10*KMAX;// Should be at least 8*KMAX if JOBU.EQ.'Y' or JOBV.EQ.'Y' and at least 2*KMAX+1 otherwise.
	int* IWORK = new int[LIWORK];

	double DOPTION[3];// = new double[3];
	DOPTION[0] = sqrt(eps/K);
	DOPTION[1] = 10*pow(eps,3.0/4.0);
	DOPTION[2] = anorm;//anorm?

	int IOPTION[2];// = new int[2];
	IOPTION[0] = 0;
	IOPTION[1] = 1;

	int INFO = -1; //okay

	// std::cout << "vor der svd " << K << " " << KMAX << std::endl;
	dlansvd_(&YES,&YES,&M,&N,&K,&KMAX,aprod,U,&LDU,pS,BND,
	V,&LDV,&TOLIN,WORK,&LWORK,IWORK,&LIWORK,DOPTION,IOPTION,
	&INFO,0,(int*)user);
	// std::cout << "nach der svd " << K << " " << KMAX << std::endl;
	

	//do some memcopy to obtain U and V
	delete[] WORK;
	delete[] IWORK;
	delete[] BND;
	// delete[] U;
	// delete[] V;
	// std::cout << INFO << std::endl;
	if(INFO==0)
		return k;
	return INFO < k ? INFO : k;
}