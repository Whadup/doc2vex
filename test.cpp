#include <sstream>
#include <iostream>
#include <fstream>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <thread>
#include <string>
#include "svd.h"
#include "args.h"
#include "dictionary.h"
#include "matrix.h"

struct UserData
{
	LowRankMatrix* l;
	LowRankMatrix* l2;
	double a=0;
	double b=0;
	SparseMatrix* s;
};

void sparseMatrixVector(const char *transa,
						int *m,
						int *n,     
						double *x,
						double *y,
						double *dparm,
						int *iparm)
{
	void* userData = (void*) iparm;
	SparseMatrix& d = *(SparseMatrix*) userData;
	sparseMatrixVector(d,x,y,*transa=='t',0);
	// std::cout << d->documentSizes->size() << std::endl;
	// if(*transa=='n')
	// {
	// 	//m rows (tokens)
	// 	//n columns (documents)
	// 	double sum = 0;
	// 	for(int i=0;i<*n;i++)
	// 		sum+=x[i];
	// 	#pragma omp parallel for
	// 	for(int i=0;i<*m;i++)
	// 	{
	// 		y[i] = sum*d->zero;
	// 		for(int j=d->tokenSizes[i];j<d->tokenSizes[i+1];j++)
	// 			y[i]+=(d->documentFrequencies[j]-d->zero)*x[d->documentIds[j]];
	// 	}
	// }
	// else if(*transa=='t')
	// {
	// 	double sum = 0;
	// 	for(int i=0;i<*m;i++)
	// 		sum+=x[i];
	// 	#pragma omp parallel for
	// 	for(int i=0;i<*n;i++)
	// 	{
	// 		y[i] = sum*d->zero;
	// 		for(int j=d->documentSizes[i];j<d->documentSizes[i+1];j++)
	// 			y[i]+=(d->tokenFrequencies[j]-d->zero)*x[d->tokenIds[j]];
	// 	}
	// }
}

void lowRankPlusSparseMatrixVector(const char *transa,
						int *m,
						int *n,     
						double *x,
						double *y,
						double *dparm,
						int *iparm)
{
	void* userData = (void*) iparm;
	//magically obtain two pointers.
	SparseMatrix& d = *((UserData*) userData)->s;
	LowRankMatrix& l = *((UserData*) userData)->l;
	sparseMatrixVector(d,x,y,*transa=='t',0);
	if(((UserData*) userData)->l2==0)
	{
		lowRankMatrixVector(l,1,x,y,*transa=='t',1);
	}
	else
	{
		lowRankMatrixVector(l,((UserData*) userData)->a,x,y,*transa=='t',1);
		LowRankMatrix& l2 = *((UserData*) userData)->l2;
		lowRankMatrixVector(l2,((UserData*) userData)->b,x,y,*transa=='t',1);
	}
}


int loadSparseDocumentMatrix(
	std::ifstream& ifs,
	std::shared_ptr<fasttext::Dictionary>& dict_,
	SparseMatrix& d
	)
{

	std::vector<int32_t> tokens;
	std::vector<int32_t> labels;
	std::minstd_rand g1(1504);
	int* wordCounts = new int[dict_->nwords()];
	for(int i=0;i<dict_->nwords();i++)
		wordCounts[i]=0;
	int ii = 0;
	do
	{
		int aa = dict_->getLine(ifs,tokens,labels,g1);  
		labels.clear();
		std::sort(tokens.begin(), tokens.end(), [](int a, int b) {return b > a;});   
		int last = -1;
		double c = 0;
		d.documentSizes.push_back(d.tokenIds.size());
		for(int t:tokens)
		{
			if(t!=last and last!=-1)
			{
				d.tokenIds.push_back(last);
				d.tokenFrequencies.push_back(c);
				wordCounts[last]++;
				c=0.0;
			}
			last = t;
			c+=1.0;
		}
		d.tokenIds.push_back(last);
		d.tokenFrequencies.push_back(c);
		wordCounts[last]++;
		// std::cout << " " << ii << std::endl;
		ii++;
	}
	while(!ifs.eof());
	d.documentSizes.push_back(d.tokenIds.size());
	int c = 0;
	for(int i=0;i<dict_->nwords();i++)
	{
		d.tokenSizes.push_back(c);
		c+=wordCounts[i];
	}
	d.tokenSizes.push_back(c);
	for(int i=0;i<c;i++)
	{
		d.documentIds.push_back(0);
		d.documentFrequencies.push_back(0.0);
	}
	
	reTranspose(ii,dict_->nwords(),d);
	delete[] wordCounts;
	ifs.close();
	return ii;
}

void printSparseDocumentMatrix(
	int ii,
	std::shared_ptr<fasttext::Dictionary>& dict_,
	SparseMatrix& d)
{
	for(int i=0;i<ii;i++)
	{
		for(int j = d.documentSizes[i];j<d.documentSizes[i+1];j++)
		{
			int token = d.tokenIds[j];
			double freq = d.tokenFrequencies[j];
			std::cout << dict_->getWord(token) << ":" << freq << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;

	for(int i=0;i<dict_->nwords();i++)
	{
		std::cout << dict_->getWord(i) << "| ";
		for(int j = d.tokenSizes[i];j<d.tokenSizes[i+1];j++)
		{
			int doc = d.documentIds[j];
			double freq = d.documentFrequencies[j];
			std::cout << doc<< ":" << freq << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}





void randomSubset(int* indices,int* result,int N,int sampleSize,int ignoreSize, int* ignore)
{
	for(int i=0;i<ignoreSize;i++)
	{
		unsigned const r = ignore[ignoreSize-1-i];
		indices[r] = indices[N-i-1];
		indices[N-i-1] = r;
		// std::cout << "ignore " << r << std::endl;
	}
	// for(int i=0;i<N;i++)
	// 	std::cout << indices[i] << ",";
	// std::cout << std::endl;
	// if(ignoreSize+sampleSize>N)
	// 	std::cerr << "das funktioniert nicht mit der samplegröße und den daten..."
	// 		<< ignoreSize
	// 		<< " " << sampleSize << " " << N 
	// 		<< std::endl;
	for (unsigned i = 0; i < sampleSize; i++)
	{
		unsigned const r = rand() % (N-ignoreSize-i);
		result[i] = indices[r];
		indices[r] = indices[N-ignoreSize-i-1];
		indices[N-ignoreSize-i-1] = r;
	}
	for (unsigned i = N-sampleSize-ignoreSize; i < N; i++)
	{
		// restore previously changed values
		indices[indices[i]] = indices[i];
		indices[i] = i;
		// std::cout << indices[i] << "";
	}
	// std::cout << std::endl;
}
void documentGradientApprox(SparseMatrix& data,
						int numberOfTokens,
						int numberOfDocuments,
						LowRankMatrix& currentSolution,
						LowRankMatrix& oldSolution,
						SparseMatrix& gradient,
						int* wordCounts,
						int doc,
						int docSize,
						double* buffer,
						double alpha,
						int softmaxSample,
						int* randomSet,
						int* sample,
						double minValueThreshold,
						bool accelerated,
						double t,
						double nt,
						double& anorm,
						double& loglikelihood,
						double& total
						)
{
	int uniqueTokens = data.documentSizes[doc+1]-data.documentSizes[doc];
	if(uniqueTokens+softmaxSample>numberOfTokens)
		softmaxSample = numberOfTokens-uniqueTokens;
	//Sample a set of unique words of size 'softaxSample'
	randomSubset(randomSet,sample,
		numberOfTokens,softmaxSample,
		uniqueTokens,&data.tokenIds[data.documentSizes[doc]]);
	// for(int i=0;i<softmaxSample;i++)
	// 	std::cout << sample[i] << " ";
	// std::cout << std::endl;
	// for(int i=0;i<uniqueTokens;i++)
	// 	std::cout << data.tokenIds[data.documentSizes[doc]+i] << " ";
	// std::cout << std::endl;
	//recover statistics for sample and tokens from low rank solution in buffer and bufferPos
	double* bufferPos = new double[uniqueTokens];
	double nmv = 5000; //useless
	double lsNeg; // = sampledLogSum(currentSolution,doc,buffer,nmv,softmaxSample,sample);
	double lsPos; //= sampledLogSum(currentSolution,doc,bufferPos,nmv,docSize,&data.tokenIds[data.documentSizes[doc]]);
	if(accelerated)
	{
		lsNeg = sampledLogSumAccelerated(currentSolution,oldSolution,1.0+(t-1)/nt,-(t-1)/nt,doc,buffer,nmv,softmaxSample,sample);	
		lsPos = sampledLogSumAccelerated(currentSolution,oldSolution,1.0+(t-1)/nt,-(t-1)/nt,doc,bufferPos,nmv,uniqueTokens,&data.tokenIds[data.documentSizes[doc]]);
	}
	else
	{
		lsNeg = sampledLogSum(currentSolution,doc,buffer,nmv,softmaxSample,sample);
		lsPos = sampledLogSum(currentSolution,doc,bufferPos,nmv,uniqueTokens,&data.tokenIds[data.documentSizes[doc]]);
	}
	// if(fabs(lsNeg)>100000)
	// {
	// 	for(int i=0;i<softmaxSample;i++)
	// 		std::cout << buffer[i] << " ";
	// 	std::cout << std::endl;
	// }
	//approximate partition function
	// double* buffer2 = new double[numberOfTokens];
	// double realZ = logSumAccelerated(currentSolution,oldSolution,1.0+(t-1)/nt,-(t-1)/nt,doc,buffer2,nmv);
	// delete[] buffer2;
	// for(int i=0;i<numberOfTokens;i++)
	// 	std::cout << buffer2[i] << " ";
	// std::cout << std::endl;
	// for(int i=0;i<uniqueTokens;i++)
	// 	std::cout << bufferPos[i] << " ";
	// for(int i=0;i<softmaxSample;i++)
	// 	std::cout << buffer[i] << " ";
	// std::cout << std::endl;
	int upsample = ((numberOfTokens-uniqueTokens)/softmaxSample);

	double Z = log(exp(lsNeg)*upsample + exp(lsPos));
	Z = lsPos + log(exp(lsNeg-lsPos) + 1); 
	// #warning "hack for upsample = 1"	
	double pLeftOver = (1 - (exp(lsNeg - Z) + exp(lsPos -Z)));
	double zero = 0;
	if(numberOfTokens-uniqueTokens-softmaxSample>0)
		zero = pLeftOver/(numberOfTokens-uniqueTokens-softmaxSample);
	gradient.zero[doc] = - alpha * docSize * zero;				
	//std::cout << lsNeg << " " << lsPos << " " << uniqueTokens << " " << softmaxSample << " " << Z << " " << gradient.zero[doc] << std::endl;
	//compute loglikelihood and gradient for the tokens of the document

	int i=0;
	for(int j=data.documentSizes[doc];j<data.documentSizes[doc+1];j++)
	{
		wordCounts[data.tokenIds[j]]++;
		const double p = exp(bufferPos[i]-Z);
		double g = -data.tokenFrequencies[j] + p * docSize;
		gradient.tokenIds.push_back(data.tokenIds[j]);
		gradient.tokenFrequencies.push_back(-alpha * g);
		gradient.documentIds.push_back(0);
		gradient.documentFrequencies.push_back(0.0);
		
		if(-log2(p)>100)
		{
			//std::cout << p << "(" << bufferPos[i] << "-" << Z << ") ";
			loglikelihood+=100*data.tokenFrequencies[j];
		}
		else
		{
			loglikelihood+=-log2(p)*data.tokenFrequencies[j];
		}
			
		total+=data.tokenFrequencies[j];
		i++;
	}
	for(int j=0;j<softmaxSample;j++)
	{
		const double p = exp(buffer[j]-Z);
		//round to default value for more sparsity
		if(fabs(p-zero)<minValueThreshold)
			continue;			
		int token = sample[j];
		wordCounts[token]++;
		double g = p * docSize;
		gradient.tokenIds.push_back(token);
		gradient.tokenFrequencies.push_back(-alpha * g);
		gradient.documentIds.push_back(0);
		gradient.documentFrequencies.push_back(0.0);
	}
	delete[] bufferPos;
	gradient.documentSizes.push_back(gradient.tokenIds.size());
}
void documentGradient(SparseMatrix& data,
						int numberOfTokens,
						int numberOfDocuments,
						LowRankMatrix& currentSolution,
						LowRankMatrix& oldSolution,
						SparseMatrix& gradient,
						int* wordCounts,
						int doc,
						int docSize,
						double* buffer,
						double alpha,
						bool accelerated,
						double t,
						double nt,
						double& anorm,
						double& loglikelihood,
						double& total,
						double& minValueThreshold
						)
{
	double nmv=100000;
	double ls;
	if(accelerated)
		ls = logSumAccelerated(currentSolution,oldSolution,1.0+(t-1)/nt,-(t-1)/nt,doc,buffer,nmv);
	else
		ls = logSum(currentSolution,doc,buffer,nmv);

	//these two loops should be merged into one to have less entries in gradient
	// for(int j=data.documentSizes[doc];j<data.documentSizes[doc+1];j++)
	// {
	// 	wordCounts[data.tokenIds[j]]++;
	// 	const double p = exp(buffer[data.tokenIds[j]]-ls);
	// 	double g = -data.tokenFrequencies[j];// 
	// 	// std::cout << g + p * docSize << std::endl;
	// 	anorm+=g*g;
	// 	gradient.tokenIds.push_back(data.tokenIds[j]);
	// 	gradient.tokenFrequencies.push_back(-alpha * g);
	// 	gradient.documentIds.push_back(0);
	// 	gradient.documentFrequencies.push_back(0.0);
	// 	// loglikelihood+=-log2(p)*data.tokenFrequencies[j];
	// 	if(-log2(p)>100)
	// 	{
	// 		//std::cout << p << "(" << bufferPos[i] << "-" << Z << ") ";
	// 		loglikelihood+=100*data.tokenFrequencies[j];
	// 	}
	// 	else
	// 	{
	// 		loglikelihood+=-log2(p)*data.tokenFrequencies[j];
	// 	}
	// 	total+=data.tokenFrequencies[j];
	// }
	int j = data.documentSizes[doc];
	int zeroCounter = 0;
	double zeroAvg = 0.0;
	for(int tok=0;tok<numberOfTokens;tok++)
	{
		double p = exp(buffer[tok]-ls);
		double g = p * docSize;
		if(j< data.documentSizes[doc+1] && tok==data.tokenIds[j])
		{
			g+=-data.tokenFrequencies[j];
			if(-log2(p)>100)
			{
				//std::cout << p << "(" << bufferPos[i] << "-" << Z << ") ";
				loglikelihood+=100*data.tokenFrequencies[j];
			}
			else
			{
				loglikelihood+=-log2(p)*data.tokenFrequencies[j];
			}
			total+=data.tokenFrequencies[j];
			j++;
		}
		anorm+=(buffer[tok]+g)*(buffer[tok]+g);

		if(g>0 && g<docSize * minValueThreshold)
		{
			zeroCounter++;
			zeroAvg+=g;
		//	continue;
		}
		// if(g>0 && g<docSize * minValueThreshold)
		// 	continue;

		wordCounts[tok]++;
		gradient.tokenIds.push_back(tok);
		gradient.tokenFrequencies.push_back(-alpha * g);
		gradient.documentIds.push_back(0);
		gradient.documentFrequencies.push_back(0.0);
	}
	if(zeroCounter>0)
		gradient.zero[doc] = -alpha * zeroAvg / zeroCounter;
	gradient.documentSizes.push_back(gradient.tokenIds.size());
}

void worker(SparseMatrix& data,
			int numberOfDocuments,
			int numberOfTokens, 
			int* documentSizes,
			int firstDoc,
			int lastDoc,
			LowRankMatrix& currentSolution,
			LowRankMatrix& oldSolution,
			SparseMatrix& gradient,
			int*& wordCounts,
			double alpha,
			int softmaxSample,
			double minValueThreshold,
			bool accelerated,
			double t,
			double nt,
			double& anorm,
			double& loglikelihood,
			double& total
			)
{
	double* buffer;
	int* randomSet = 0;
	int* sample = 0;
	if(softmaxSample==0) 
		buffer = new double[numberOfTokens];
	else 
	{
		buffer = new double[softmaxSample];
		randomSet = new int[numberOfTokens];
		for(int i=0;i<numberOfTokens;i++)
			randomSet[i]=i;
		sample = new int[softmaxSample];
	}
	
	for(int i=0;i<numberOfTokens;i++)
		wordCounts[i] = 0;
	anorm = 0;
	loglikelihood = 0;
	total = 0;
	gradient.documentSizes.push_back(0);
	gradient.zero = new double[numberOfDocuments];
	for(int i=0;i<numberOfDocuments;i++)
		gradient.zero[i] = 0;
	for(int doc=firstDoc;doc<lastDoc;doc++)
	{
		// std::cout << doc << " ";
		// int unique = data.documentSizes[doc+1]-data.documentSizes[doc];
		if(softmaxSample>0)
		{
			documentGradientApprox(data,numberOfTokens,numberOfDocuments,
				currentSolution,oldSolution,
				gradient,
				wordCounts,
				doc,documentSizes[doc],
				buffer,
				alpha,
				softmaxSample,
				randomSet,
				sample,
				minValueThreshold,
				accelerated,
				t,nt,
				anorm,
				loglikelihood,
				total
				);
		}
		else if(softmaxSample==0)
		{
			documentGradient(data,numberOfTokens,numberOfDocuments,
				currentSolution,oldSolution,
				gradient,
				wordCounts,
				doc,documentSizes[doc],
				buffer,
				alpha,
				accelerated,
				t,nt,
				anorm,
				loglikelihood,
				total,
				minValueThreshold
				);
		}
	}
	delete[] buffer;
	if(randomSet) delete[] randomSet;
	if(sample) delete[] sample;
}

int main(int argc, char** argv)
{
	bool SAFETYCHECK = false;
	std::cout << std::fixed << std::setprecision(8);
	if(argc==1)
		exit(0);
	std::shared_ptr<fasttext::Args> a = std::make_shared<fasttext::Args>();
	a->parseArgs(argc, argv);

	auto dict_ = std::make_shared<fasttext::Dictionary>(a);
	if (a->input == "-") {
		// manage expectations
		std::cerr << "Cannot use stdin for training!" << std::endl;
		exit(EXIT_FAILURE);
	}
	if(!a->infer)
	{
		std::ifstream ifs(a->input);
		if (!ifs.is_open()) {
			std::cerr << "Input file cannot be opened!" << std::endl;
			exit(EXIT_FAILURE);
		}
	
		dict_->readFromFile(ifs);
		

		SparseMatrix data;
		
		int numberOfDocuments = loadSparseDocumentMatrix(ifs,dict_,data);
		int numberOfTokens = dict_->nwords();
		int maxRank = 2000;
		if(numberOfDocuments < maxRank)
			maxRank = numberOfDocuments;
		if(numberOfTokens < maxRank)
			maxRank = numberOfTokens;

		std::cout << numberOfDocuments << "x" << numberOfTokens << std::endl;
		std::cout << data.documentIds.size() << " <-> " << data.tokenIds.size() << std::endl;
		
		SparseMatrix init;
		duplicateSparseMatrix(data,init);

		int* documentSizes = new int[numberOfDocuments];
		double bayesOptimal = 0;
		double denom = 0;
		double dsquared = 0;
		double maxd = 0;
		//compute lipschitz constant and document sizes and optimal full rank solution.
		for(int i=0;i<numberOfDocuments;i++)
		{
			dsquared += (data.documentSizes[i]-data.documentSizes[i+1])*(data.documentSizes[i]-data.documentSizes[i+1]);
			double n=0;
			for(int j = data.documentSizes[i];j<data.documentSizes[i+1];j++)
				n+=data.tokenFrequencies[j];
			documentSizes[i]=n;
			if(documentSizes[i]>maxd)
				maxd = documentSizes[i];
			//while we're at it, lets compute the optimal solution, too
			for(int j = data.documentSizes[i];j<data.documentSizes[i+1];j++)
			{
				init.tokenFrequencies[j] = log(1.0*data.tokenFrequencies[j]/n);
				bayesOptimal+=data.tokenFrequencies[j] * -log2(data.tokenFrequencies[j]/n);
				denom+=data.tokenFrequencies[j];
				// std::cout << tokenFrequencies[j] << " ";
			}
		}



		double L = sqrt(numberOfTokens-1)/numberOfTokens * maxd;
		std::cout << "Lipschitz "<< L << " " << maxd << " " << sqrt(dsquared) << std::endl; 
		std::cout << "Bayes Optimal Model "<<bayesOptimal/denom << std::endl;

		init.zero=new double[numberOfDocuments];
		for(int i=0;i<numberOfDocuments;i++)
			init.zero[i]=-8;
		reTranspose(numberOfDocuments,numberOfTokens,init);
		// delete[] init.zero;

		// printSparseDocumentMatrix(numberOfDocuments,dict_,data);

		double anorm = 0;
		for(double i : init.tokenFrequencies) anorm+=i*i;
		anorm = sqrt(anorm);
		std::cout << "anorm "<< anorm << std::endl;
		// anorm = 1; //OMG WAS TU TDAS
		#warning "anorm total dumm gesetzt"
		int initialRank = a->initialRank;
		LowRankMatrix oldSolution;
		LowRankMatrix currentSolution;
		makeLowRankMatrix(currentSolution,numberOfTokens,numberOfDocuments,initialRank);
		//probably shoud use a different initialization
		// std::cout << "Computing initial rank "<<initialRank << " solution" << std::endl;
		currentSolution.k = svd(numberOfTokens,numberOfDocuments,initialRank,&sparseMatrixVector,
			 currentSolution.U,currentSolution.V,currentSolution.S,anorm,(void*)&init);
		for(int i=0;i<currentSolution.k;i++)
			std::cout << currentSolution.S[i] << " ";
		std::cout << std::endl;


		// std::cout << "on second thought...lets just go random "<<std::endl;
		// for(int i=0;i<initialRank;i++)
		// 	currentSolution.S[i]=1;
		
		// std::random_device rd;  //Will be used to obtain a seed for the random number engine
	 //    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	 //    std::uniform_real_distribution<> dis(-0.5, 0.5);

		// for(int j=0;j<initialRank*numberOfTokens;j++)
		// 	currentSolution.U[j] = dis(gen);
		// for(int j=0;j<initialRank*numberOfDocuments;j++)
		// 	currentSolution.V[j] = dis(gen);

		const int k=500;	
		double minValueThreshold = 1.0/numberOfTokens;
		
		// double* buffer = new double[numberOfTokens];


		int softmaxSample = a->softmaxSample;
		
		// int* randomSet;
		// int* sample;
		// if(softmaxSample>0)
		// {
		// 	delete[] buffer;
		// 	buffer = new double[softmaxSample];
		// 	randomSet = new int[numberOfTokens];
		// 	for(int i=0;i<numberOfTokens;i++)
		// 		randomSet[i]=i;
		// 	sample = new int[softmaxSample];

		// }
		double alpha = 0.5/L;
		double lambda = a->lambda*alpha;
		double* weights = new double[maxRank];
		// int center = 200;
		// int width = 75;
		for(int i=0;i<maxRank;i++)
		{	
			if(a->regularizer == fasttext::regularizer_name::constant)
				weights[i]=lambda;
			else if(a->regularizer == fasttext::regularizer_name::step)
			{	
				if(i>a->center)
					weights[i]=a->lambda*alpha;
				else
					weights[i]=1.0/a->lambda*alpha;
			}
			else if(a->regularizer == fasttext::regularizer_name::linear)
				weights[i]=lambda * (1+i);
			else
				weights[i]=lambda/(1+exp(-6.0/a->width * (i-a->center)));///25.0));
			std::cout << weights[i] << " ";
		}
		std::cout << std::endl;
		double oldNorm = 0;
		for(int i=0;i<currentSolution.k;i++)
			oldNorm+=weights[i]/alpha * currentSolution.S[i];
		
		
		std::cout << "Soft Thresholding at "<< lambda << std::endl;

		bool accelerated = false;
		double t = 1;

		const int numWorkers = 40;
		int* firstDocs = new int[numWorkers];
		int* lastDocs = new int[numWorkers];
		int jobSize = ceil(1.0*numberOfDocuments/numWorkers);
		for(int i=0;i<numWorkers;i++)
		{
			firstDocs[i] = i*jobSize;
			lastDocs[i] = (i+1) * jobSize;
			if(lastDocs[i]>numberOfDocuments)
				lastDocs[i] = numberOfDocuments;
		}
		double* loglikelihoods = new double[numWorkers];
		double* totals = new double[numWorkers];
		double* anorms = new double[numWorkers];
		int** wordCounts = new int*[numWorkers];
		for(int i=0;i<numWorkers;i++)
			wordCounts[i] = new int[numberOfTokens];
		for(int iter=0;iter<a->epoch;iter++)
		{
			// softmaxSample=std::ceil(softmaxSample*1.03);
			if(iter==a->epoch-1)
			{
				softmaxSample = 0;
				std::cout << "LAST EPOCHE IS EXACT!";
			}

			if(softmaxSample>numberOfTokens)
				softmaxSample=numberOfTokens;
			std::cout << "Iteration " << iter << std::endl;
			double nt = (1.0 + sqrt(1+4*t*t)) / 2;	
			SparseMatrix* gradients = new SparseMatrix[numWorkers];
			#pragma omp parallel for
			for(int i=0;i<numWorkers;i++)
			{
				worker(data,
						numberOfDocuments,
						numberOfTokens, 
						documentSizes,
						firstDocs[i],
						lastDocs[i],
						currentSolution,
						oldSolution,
						gradients[i],
						wordCounts[i],
						alpha,
						softmaxSample,
						minValueThreshold,
						accelerated && iter>0,
						t,
						nt,
						anorms[i],
						loglikelihoods[i],
						totals[i]
				);
			}
			minValueThreshold*=0.997;

			double anorm = 0;
			double loglikelihood = 0;
			double total = 0;
			SparseMatrix gradient;
			gradient.zero = new double[numberOfDocuments];
			gradient.documentSizes.push_back(0);
			int offset = 0;
			for(int i=0;i<numWorkers;i++)
			{
				//std::cout << gradients[i].tokenIds.size() << " " << gradients[i].documentSizes.size() << std::endl;
				//printSparseDocumentMatrix(numberOfDocuments,dict_,gradients[i]);
				for(int j = firstDocs[i];j<lastDocs[i];j++)
					gradient.zero[j]=gradients[i].zero[j];
				delete[] gradients[i].zero;
				for(int j = 0;j<gradients[i].tokenIds.size();j++)
				{
					gradient.tokenIds.push_back(gradients[i].tokenIds[j]);
					gradient.tokenFrequencies.push_back(gradients[i].tokenFrequencies[j]);
					gradient.documentIds.push_back(gradients[i].documentIds[j]);
					gradient.documentFrequencies.push_back(gradients[i].documentFrequencies[j]);
				}
				for(int j = 1;j<gradients[i].documentSizes.size();j++)
					gradient.documentSizes.push_back(offset+gradients[i].documentSizes[j]);
				offset = gradient.documentSizes[gradient.documentSizes.size()-1];
				anorm += anorms[i];
				loglikelihood += loglikelihoods[i];
				total += totals[i];
				if(i>0)
				{
					for(int j = 0;j<numberOfTokens;j++)
						wordCounts[0][j] += wordCounts[i][j];
					//delete[] wordCounts[i];
				}
			}
			delete[] gradients;
			
			anorm = sqrt(anorm);
			int c = 0;
			for(int i=0;i<dict_->nwords();i++)
			{
				gradient.tokenSizes.push_back(c);
				c+=wordCounts[0][i];
			}
			gradient.tokenSizes.push_back(c);

			reTranspose(numberOfDocuments,numberOfTokens,gradient);
			if(false)
			{
				std::ofstream myfile("G.csv");
				for(int i=0;i<numberOfDocuments;i++)
				{	for(int j=gradient.documentSizes[i];j<gradient.documentSizes[i+1];j++)
					{
						if(j>gradient.documentSizes[i]) myfile << ",";
						myfile << gradient.tokenFrequencies[j];
					}
					myfile << std::endl;
				}
				myfile.close();
			}
			//std::cout << gradient.tokenIds.size() << " " << gradient.documentSizes.size() << std::endl;
			//printSparseDocumentMatrix(numberOfDocuments,dict_,gradient);
			if(softmaxSample==0)
				std::cout << "Current Loss "<<(loglikelihood/total) << std::endl;
			else	
				std::cout << "Estimated Loss "<<(loglikelihood/total) << std::endl;
			std::cout << "Computed Sparse Gradient with " << gradient.tokenIds.size() << " non-zeros" << std::endl;
			//do some svd
			//check if svd was sucessful with the rank supplied...if not increase and repeat
			//thresholdd
			UserData d;
			d.s = &gradient;
			d.l = &currentSolution;
			if(accelerated && iter>0)
			{
				d.l2 = &oldSolution;
				d.b = -(t-1)/nt;
				d.a = 1.0+(t-1)/nt;
			}
			else
			{
				d.a = 1.0;
				d.b = 0.0;
				d.l2 = 0; 
			}



			LowRankMatrix newSolution;
			newSolution.k = currentSolution.k*1.25;
			if(newSolution.k>maxRank)
				newSolution.k=maxRank;
			anorm+=currentSolution.S[0];
			makeLowRankMatrix(newSolution,numberOfTokens,numberOfDocuments,newSolution.k);
			std::cout << "compute svd with rank "<< newSolution.k << std::endl;
			int resultK = svd(numberOfTokens,numberOfDocuments,newSolution.k,&lowRankPlusSparseMatrixVector,
			newSolution.U,newSolution.V,newSolution.S,anorm,(void*)&d);
			// std::cout << "svd is fertig" << std::endl;
			while(resultK==-1)
			{
				anorm*=2; //seems to help for some reason...
				std::cout << "recompute svd with larger anorm" << std::endl;
				resultK = svd(numberOfTokens,numberOfDocuments,newSolution.k,&lowRankPlusSparseMatrixVector,
					newSolution.U,newSolution.V,newSolution.S,anorm,(void*)&d);
			}
			newSolution.k = resultK;
			// for(int i=0;i<newSolution.k;i++)
			// 	std::cout << std::fixed << newSolution.S[i] << "\t";
			// std::cout << std::endl;
			int k=newSolution.k*2;
			
			while(newSolution.S[newSolution.k-1]>weights[newSolution.k-1] && newSolution.k<maxRank)
			{
				if(k>maxRank)
					k=maxRank;
				std::cout << "recompute svd " << newSolution.S[newSolution.k-1] << " with rank " << k << std::endl;
				deleteLowRankMatrix(newSolution);
				
				makeLowRankMatrix(newSolution,numberOfTokens,numberOfDocuments,k);
				newSolution.k = svd(numberOfTokens,numberOfDocuments,newSolution.k,&lowRankPlusSparseMatrixVector,
				newSolution.U,newSolution.V,newSolution.S,anorm,(void*)&d);
				if(newSolution.k==maxRank)
					break;
				k*=2;
				
				// for(int i=0;i<newSolution.k;i++)
				// 	std::cout << std::fixed << newSolution.S[i] << "\t";
				// std::cout << std::endl;
			}

			for(int i=newSolution.k-1;i>=0;i--)
			{
				newSolution.S[i]-=weights[i];
				if(newSolution.S[i]<=0)
				{
					newSolution.S[i]=0;
					newSolution.k = i;
				}
			}
			double regularizer = 0;
			for(int i=0;i<newSolution.k;i++)
				regularizer+=weights[i]/alpha*newSolution.S[i];
			if(softmaxSample==0)
				std::cout << "Current Objective "<<(loglikelihood)+oldNorm << std::endl;
			else	
				std::cout << "Estimated Objective "<<(loglikelihood)+oldNorm << std::endl;

			oldNorm = regularizer;

			if(SAFETYCHECK)
			{
				double* A = new double[numberOfTokens*numberOfDocuments];
				for(int i=0;i<numberOfTokens;i++)
				{
					for(int j=0;j<numberOfDocuments;j++)
					{
						A[j*numberOfTokens+i]=0;
						for(int kk=0;kk<currentSolution.k;kk++)
						{
							//A[j*m+i]+=l.S[kk]*l.U[i+kk*m] * l.V[j+kk*n];
							A[j*numberOfTokens+i]+=currentSolution.S[kk]*currentSolution.U[i+kk*currentSolution.m] * currentSolution.V[j+kk*currentSolution.n];
						}
						for(int kk=0;kk<newSolution.k;kk++)
						{
							A[j*numberOfTokens+i]-=newSolution.S[kk]*newSolution.U[i+kk*currentSolution.m] * newSolution.V[j+kk*currentSolution.n];
						}
					}
				}

				for(int i=0;i<numberOfDocuments;i++)
				{
					for(int j=gradient.documentSizes[i];j<gradient.documentSizes[i+1];j++)
					{
						A[i*numberOfTokens+gradient.tokenIds[j]]+=gradient.tokenFrequencies[j];
					}
				}
				double delta = 0;
				for(int i=0;i<numberOfTokens;i++)
				{
					for(int j=0;j<numberOfDocuments;j++)
					{
						delta+=A[j*numberOfTokens+i]*A[j*numberOfTokens+i];
					}
				}
				delete[] A;
				std::cout << "DELTA " << sqrt(delta) << std::endl;
			}
			delete[] gradient.zero;
			deleteLowRankMatrix(oldSolution);
			oldSolution=currentSolution;
			currentSolution = newSolution;
			std::cout << "Rank of Solution " << currentSolution.k << " "<< currentSolution.S[0] << " - " << currentSolution.S[currentSolution.k-1] << std::endl;
			t=nt;
		}
		delete[] anorms;
		delete[] loglikelihoods;
		delete[] totals;
		delete[] firstDocs;
		delete[] lastDocs;
		for(int i=0;i<numWorkers;i++)
			delete[] wordCounts[i];
		delete[] wordCounts;


		

		//do some funky iterations
		if(a->output!="")
		{
			std::ofstream myfile(a->output+"SV.csv");
			for(int j=0;j<numberOfDocuments;j++)
			{
				for(int i=0;i<currentSolution.k;i++)
				{
					if(i) myfile << ",";
					myfile << currentSolution.V[j+i*currentSolution.n] * sqrt(currentSolution.S[i]);
				}
				myfile << std::endl;
			}
			myfile.close();
			std::ofstream myfile2(a->output+"US.csv");
			for(int j=0;j<numberOfTokens;j++)
			{
				for(int i=0;i<currentSolution.k;i++)
				{
					if(i) myfile2 << ",";
					myfile2 << currentSolution.U[j+i*currentSolution.m] * sqrt(currentSolution.S[i]);
				}
				myfile2 << std::endl;
			}
			myfile2.close();
			std::ofstream myfile4(a->output+"S.csv");

			for(int i=0;i<currentSolution.k;i++)
			{
				if(i) myfile4 << ",";
				myfile4 << currentSolution.S[i];
			}
			myfile4 << std::endl;
			
			myfile4.close();
			std::ofstream myfile3(a->output+".dict");
			dict_->save(myfile3);
			myfile3.close();
		}
		{
			std::ofstream myfile("V.csv");
			for(int j=0;j<numberOfDocuments;j++)
			{
				for(int i=0;i<currentSolution.k;i++)
				{
					if(i) myfile << ",";
					myfile << currentSolution.V[j+i*currentSolution.n];
				}
				myfile << std::endl;
			}
			myfile.close();
		}

		for(int i=0;i<currentSolution.k;i++)
			std::cout << std::fixed << currentSolution.S[i] << "\t";
		std::cout << "(" << currentSolution.k << ")" << std::endl;
		deleteLowRankMatrix(currentSolution);
		deleteLowRankMatrix(oldSolution);

		delete[] documentSizes;
		delete[] weights;
	}
	else
	{
		std::ifstream ifs2(a->model+".dict");
		if (!ifs2.is_open()) {
			std::cerr << "Model file cannot be opened: "<< a->model << ".dict" << std::endl;
			exit(EXIT_FAILURE);
		}
		dict_->load(ifs2);
		ifs2.close();
		std::ifstream ifs(a->input);
		SparseMatrix data;
		int numberOfDocuments = loadSparseDocumentMatrix(ifs,dict_,data);
		int numberOfTokens = dict_->nwords();
		std::cout << numberOfDocuments << " " << numberOfTokens << std::endl;

		std::vector<double> wordEmbeddings;
		std::ifstream words(a->model+"US.csv");
		int k=0;
		for(int i=0;i<numberOfTokens;i++)
		{
			std::string line;
			std::getline(words,line);
			if (line.size() <= 1) 
				break;
			std::stringstream ss(line);

			std::string entry;    
			k = 0;
			while( std::getline(ss, entry,',') ) 
			{
				if (entry.size() > 0) 
				{
					wordEmbeddings.push_back(atof(entry.c_str()));
					k++;
				}
			}
		}

		std::vector<double> s;
		std::ifstream sigma(a->model+"S.csv");
		
		for(int i=0;i<k;i++)
		{
			std::string line;
			std::getline(sigma,line);
			if (line.size() <= 1) 
				break;
			std::stringstream ss(line);

			std::string entry;    
			while( std::getline(ss, entry,',') ) 
			{
				if (entry.size() > 0) 
				{
					s.push_back(atof(entry.c_str()));
				}
			}
		}

		std::vector<double> docEmbeddings;
		std::ifstream docs(a->model+"SV.csv");
		std::string line;
		int N = 0;
		while ( std::getline(docs,line))
		{
			if (line.size() <= 1) 
				break;
			std::stringstream ss(line);
			N++;
			std::string entry;    
			while( std::getline(ss, entry,',') ) 
			{
				if (entry.size() > 0) 
				{
					docEmbeddings.push_back(atof(entry.c_str()));
				}
			}
		}
		std::cout << N << std::endl;
		N--;
		double norm = 0;
		for(double a : wordEmbeddings)
			norm+=a*a;
		norm = sqrt(norm);
		double alpha = 1.0/(norm * sqrt(numberOfTokens-1)/numberOfTokens);
		std::cout << alpha << std::endl;
		alpha = a->lr;
		std::cout << alpha << std::endl;

		std::cout << k << std::endl;

		//Load Word Embeddings
		//Do the magic
		LowRankMatrix solution;
		makeLowRankMatrix(solution,numberOfTokens,N+1,k);
		for(int i=0;i<k;i++)
		{
			solution.S[i]=s[i];
			std::cout << s[i] << " ";
		}
		std::cout << std::endl;
		for(int i=0;i<numberOfTokens;i++)
			for(int j=0;j<k;j++)
			{
				solution.U[i+j*solution.m] = wordEmbeddings[i*k+j]/sqrt(s[j]);
				// std::cout << solution.U[i+j*solution.m] << " ";
			}
		for(int i=0;i<N;i++)
			for(int j=0;j<k;j++)
				solution.V[j*solution.n+i]=docEmbeddings[i*k+j]/sqrt(s[j]);
		for(int j=0;j<k;j++)
			solution.V[j*solution.n+N]=0;
		double* buffer = new double[numberOfTokens];
		
		// LowRankMatrix initMatrix;
		// makeLowRankMatrix(initMatrix,numberOfTokens,N+1,k);

		if(a->output!="")
		{
			std::ofstream myfile(a->output+"IV.csv");
			for(int doc=0;doc<numberOfDocuments;doc++)
			{
				double documentSize = 0;
				for(int j=data.documentSizes[doc];j<data.documentSizes[doc+1];j++)
				{
					documentSize+=data.tokenFrequencies[j];
				}

				double* y = new double[numberOfTokens];
				for(int j = 0;j<numberOfTokens;j++)
					y[j] = -8;
				for(int j = data.documentSizes[doc];j<data.documentSizes[doc+1];j++)
				{
					y[data.tokenIds[j]]=log(1.0*data.tokenFrequencies[j]/documentSize);
				}
				for(int j=0;j<k;j++)
				{
                    solution.V[j*solution.n+N]=0;
                    for(int tok = 0;tok<numberOfTokens;tok++)
                    {
                    	  solution.V[j*solution.n+N]+= 1.0/solution.S[j] * y[tok] * solution.U[tok+j*solution.m];

                    }
                    //std::cout << solution.V[j*solution.n+N] << " ";
				}
				//std::cout << std::endl;
				delete[] y;
				//solution.V[N] = -1.45;
				std::cout << "Document: " << doc << std::endl;
				double loss = 0;
				double nmv;
				
				
				for(int i=0;i<a->epoch;i++)
				{
					loss = 0;
					double ls = logSum(solution,N,buffer,nmv);
					int j = data.documentSizes[doc];
					for(int tok=0;tok<numberOfTokens;tok++)
					{				
						double p = exp(buffer[tok]-ls);
						// std::cout << p << " ";
						if(j< data.documentSizes[doc+1] && tok==data.tokenIds[j])
						{
							//do the gradient magic
							//add embedding of tok to document embedding
							//#pragma omp parallel for
							for(int kk=0;kk<k;kk++)
							{
								solution.V[N+solution.n*kk]-= alpha * data.tokenFrequencies[j] * -solution.U[kk*solution.m+tok] * solution.S[kk] / documentSize;
							}
							if(-log2(p)>100)
								loss+=100*data.tokenFrequencies[j];
							else
								loss+=-log2(p)*data.tokenFrequencies[j];
							j++;
						}
						//do the gradient magic: subtract word embedding times p from doc embedding
						//#pragma omp parallel for
						for(int kk=0;kk<k;kk++)
						{
							solution.V[N+solution.n*kk]-= alpha * p * solution.S[kk] * solution.U[kk*solution.m+tok];
						}
					}
					loss/=documentSize;
					if(i%100==99)
						std::cout << loss << " " << std::flush;
					// std::cout << "("<<solution.V[N]<<") ";
					// if(solution.V[N]*sqrt(solution.S[0]) <-1.4)
					// 	break;
				}
				std::cout << loss << " " << documentSize << " " << solution.V[N] * sqrt(solution.S[0]) << std::endl;
				//write document to file
				for(int i=0;i<solution.k;i++)
				{
					if(i) myfile << ",";
					myfile << solution.V[N+i*solution.n] * sqrt(solution.S[i]);
				}
				myfile << std::endl;
			}
			
			
			myfile.close();
			}
			else
			{
				exit(0);
			}


		
		
		delete[] buffer;
		deleteLowRankMatrix(solution);
	}
	
}
