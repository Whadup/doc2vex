#include "matrix.h"
#include <iostream>
#include <random>
#include <cmath>
int main()
{
	int m = 20;
	int n = 15;
	int k = 10;
	LowRankMatrix l;
	makeLowRankMatrix(l,m,n,k);
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(-0.5, 0.5);
	for(int i=0;i<k;i++)
		l.S[i]=1+dis(gen);
	for(int j=0;j<k*m;j++)
		l.U[j] = dis(gen);
	for(int j=0;j<k*n;j++)
		l.V[j] = dis(gen);

	double* A = new double[m*n];

	for(int i=0;i<m;i++)
	{
		for(int j=0;j<n;j++)
		{
			A[j*m+i]=0;
			for(int kk=0;kk<k;kk++)
			{
				A[j*m+i]+=l.S[kk]*l.U[i+kk*m] * l.V[j+kk*n];
			}
			// std::cout << A[j*m+i] << " ";
		}
		// std::cout << std::endl;
	}


	double* x = new double[n]{0};
	for(int i=0;i<n;i++)
		x[i]=dis(gen);
	double* y = new double[m]{0};

	lowRankMatrixVector(l,x,y,0,0);
	for(int i=0;i<m;i++)
		std::cout << y[i] << " ";
	std::cout << std::endl;
	for(int i=0;i<m;i++)
	{
		double s = 0;
		for(int j=0;j<n;j++)
			s+=A[j*m+i] * x[j];
		std::cout << s << " ";
	}
	std::cout << std::endl;

	for(int i=0;i<m;i++)
	 	y[i]=dis(gen);

	lowRankMatrixVector(l,y,x,1,0);
	for(int i=0;i<n;i++)
		std::cout << x[i] << " ";
	std::cout << std::endl;
	for(int i=0;i<n;i++)
	{
		double s = 0;
		for(int j=0;j<m;j++)
		{
			s+=A[i*m+j] * y[j];
		}
		std::cout << s << " ";
	}
	std::cout << std::endl;

	double* buffer = new double[m];
	for(int i=0;i<n;i++)
	{
		double sum = 0;
		for(int j=0;j<m;j++)
			sum+=exp(A[i*m+j]);
		std::cout << log(sum) << std::endl;
		double d = 0;

		std::cout << logSum(l,  i,buffer, d) << std::endl;
	}
	delete[] buffer;
}