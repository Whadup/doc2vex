#include "matrix.h"
#include <iostream>
#include <random>
int main()
{
	int m = 10;
	int n = 5;
	double* A = new double[m*n];
	SparseMatrix s;
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(-0.5, 0.5);
    int* counter = new int[m]{0};
	for(int j=0;j<n;j++)
	{
		s.documentSizes.push_back(s.tokenIds.size());
		for(int i=0;i<m;i++)
		{
			A[j*m+i] = dis(gen);
			if(A[j*m+i]<0)
				A[j*m+i]=0;
			else
			{
				s.tokenIds.push_back(i);
				s.tokenFrequencies.push_back(A[j*m+i]);
				counter[i]++;
				s.documentIds.push_back(0);
				s.documentFrequencies.push_back(0);
			}
		}
	}
	s.documentSizes.push_back(s.tokenIds.size());
	int c = 0;
	for(int i=0;i<m;i++)
	{
		s.tokenSizes.push_back(c);
		c+=counter[i];
	}
	s.tokenSizes.push_back(c);
	delete[] counter;
	reTranspose(n,m,s);
	s.zero=new double[n];
	for(int i=0;i<n;i++)
		s.zero[i]=i;

	double* x = new double[n]{0};
	for(int i=0;i<n;i++)
		x[i]=dis(gen);
	double* y = new double[m]{0};

	sparseMatrixVector(s,x,y,0,0);
	for(int i=0;i<m;i++)
		std::cout << y[i] << " ";
	std::cout << std::endl;
	for(int i=0;i<m;i++)
	{
		double s = 0;
		for(int j=0;j<n;j++)
			if(A[j*m+i]==0)
				s+=x[j]*j;
			else
				s+=A[j*m+i] * x[j];
		std::cout << s << " ";
	}
	std::cout << std::endl;

	for(int i=0;i<m;i++)
		y[i]=dis(gen);

	sparseMatrixVector(s,y,x,1,0);
	for(int i=0;i<n;i++)
		std::cout << x[i] << " ";
	std::cout << std::endl;
	for(int i=0;i<n;i++)
	{
		double s = 0;
		for(int j=0;j<m;j++)
		{
			if(A[i*m+j]==0)
				s+=y[j]*i;
			else
				s+=A[i*m+j] * y[j];
		}
		std::cout << s << " ";
	}
	std::cout << std::endl;

}