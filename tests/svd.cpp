#include "matrix.h"
#include "svd.h"
#include <iostream>
#include <random>
#include <cmath>

void lowRankMatrixVector(const char *transa,
						int *m,
						int *n,     
						double *x,
						double *y,
						double *dparm,
						int *iparm)
{
	void* userData = (void*) iparm;
	//magically obtain two pointers.
	LowRankMatrix l = *((LowRankMatrix*) userData);
	lowRankMatrixVector(l,x,y,*transa=='t',0);
}

int main()
{
	int m = 5;
	int n = 5;
	int k = 3;
	LowRankMatrix l;
	makeLowRankMatrix(l,m,n,k);
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(-0.5, 0.5);
	for(int i=0;i<k;i++)
		l.S[i]=1+dis(gen);
	for(int j=0;j<k*m;j++)
		l.U[j] = dis(gen);
	for(int j=0;j<k*n;j++)
		l.V[j] = dis(gen);

	double* A = new double[m*n];

	for(int i=0;i<m;i++)
	{
		for(int j=0;j<n;j++)
		{
			A[j*m+i]=0;
			for(int kk=0;kk<k;kk++)
			{
				A[j*m+i]+=l.S[kk]*l.U[i+kk*m] * l.V[j+kk*n];
			}
			// std::cout << A[j*m+i] << " ";
		}
		// std::cout << std::endl;
	}

	LowRankMatrix s;
	makeLowRankMatrix(s,m,n,5);

	s.k=svd(m,n,s.k,&lowRankMatrixVector,
		s.U,s.V,s.S,0,(void*)&l);

	for(int i=0;i<s.k;i++)
		std::cout << s.S[i] << " ";
	std::cout << std::endl;

	double* B = new double[m*n];
	for(int i=0;i<m;i++)
	{
		for(int j=0;j<n;j++)
		{
			B[j*m+i]=0;
			for(int kk=0;kk<s.k;kk++)
			{
				B[j*m+i]+=s.S[kk]*s.U[i+kk*m] * s.V[j+kk*n];
			}
		}
	}
	double delta = 0;
	for(int i=0;i<m;i++)
		for(int j=0;j<n;j++)
			delta+=fabs(B[j*m+i]-A[j*m+i]);
	std::cout << delta << std::endl;
}